// 1.) Define a function named countLetter() that takes in two arguments: a letter and a string. It will either return the number of times the letter occurred in the string or undefined if the letter is not in the string.




function countLetter(letter, sentence) {
    let result = 0;
    if(letter.length===1){
    for(var i=0;i<sentence.length;i++){
    if(sentence[i]===letter){
    result++;
    }
    }
    if(result===0){ return "undefined"; }
    else{ return result; }
    }
    else{ return "undefined"; }
    
}



// 2.) Define a function named isIsogram() that takes in a single string as an argument and checks for any repeating characters in it. If no duplicate characters found, return true. Otherwise, return false.

function isIsogram(text) {

var res = text.toUpperCase();

var countRepeat=0;
for(var i=0; i<res.length; i++){
for(var j=0; j<res.length; j++){
if(res[i]===res[j]){ countRepeat++; }
}
}
if(countRepeat>1){
return false;
}
else{
return true
}
}


// 3.) Students aged 13 to 21 years old as well as senior citizens (65 and up) both enjoy a 20% discount on purchased goods. Students below 13 years of age cannot purchase without their parent's consent. Given an age and a price, create a function named purchase() that will output a warning if age is below permitted age or the final purchase price otherwise.


function purchase(age, price) {

   if(age>=13){

   if(age<=21||age>=65){
   return (0.8*price).toFixed(2);
   }

   else{
   return price.toFixed(2);
   }

   }
   
}


// 4.) Given an array of inventory items, create a function named findHotCategories() that will return an array of DISTINCT categories that have at least ONE of its items sold out. If all items are in stock, return a notification message instead.

function findHotCategories(items) {

    if(stocks = 0){
        return console.log(items[3])
    }



    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

// 5.) Define a function named findFlyingVoters() that takes in two arrays of strings representing voter ID’s as its arguments. This function will return an array of all voter ID’s found on both array arguments.

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

//Please do not change or remove the code below
module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};